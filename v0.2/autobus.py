# -*- coding: utf-8 -*-
from excepcions import *
from bitllet import *
from funcions_classes import *

class Autobus:
	def __init__(self, capacitat, nom):
		self.setCapacitat(capacitat)
		self.setPlacesLliures(capacitat)
		self.setNom(nom)
		self.__llista_bitllets = []
	def setCapacitat(self, capacitat):
		self.__capacitat = capacitat
	def getCapacitat(self):
		return self.__capacitat
	def setPlacesLliures(self, places_lliures):
		self.__places_lliures = places_lliures
	def getPlacesLliures(self):
		return self.__places_lliures
	def setNom(self, nom):
		self.__nom = nom
	def getNom(self):
		return self.__nom
	def getLlistaBitllets(self):
		return self.__llista_bitllets
	def afegirBitllet(self, bitllet):
		self.__llista_bitllets.append(bitllet)
	def eliminarBitllet(self, id):
		for index, bitllet in enumerate(self.getLlistaBitllets()):
			if bitllet.getId() == id:
				del self.getLlistaBitllets()[index]
	def getIds(self):
		ids = []
		for bitllet in self.__llista_bitllets:
			ids.append(bitllet.getId())
		return ids
	def __str__(self):
		return "Capacitat autobus: " + str(self.getCapacitat()) + \
				" // Places ocupades: " + str(self.getCapacitat() - self.getPlacesLliures()) + \
				" // Places lliures: " + str(self.getPlacesLliures())
	def venta(self, quantitat, noms):
		places_lliures_actualitzades = self.__places_lliures - quantitat
		self.setPlacesLliures(places_lliures_actualitzades)
		#Assignar ids: els ids venuts han de ser numeros superiors al màxim dels ids actuals:
		max_id = maximId(self.getLlistaBitllets())
		for index, nom_nou in enumerate(noms):
			id = max_id + 1 + index
			bitllet = Bitllet(nom_nou, id)
			# Funció afegirBitllet:
			self.afegirBitllet(bitllet)
	def devolucio(self, quantitat, ids):
		places_lliures_actualitzades = self.getPlacesLliures() + quantitat
		self.setPlacesLliures(places_lliures_actualitzades)
		for id in ids:
			self.eliminarBitllet(id)