def maximId(llista_bitllets):
	max_id = 0
	for bitllet in llista_bitllets:
		if bitllet.getId() > max_id:
			max_id = bitllet.getId()
	return max_id