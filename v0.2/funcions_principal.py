# -*- coding: utf-8 -*-
from autobus import *

def crear_autobusos():
	bus_metropolita = Autobus(60, "Autobús Metropolità")
	bus_interestatal = Autobus(100, "Autobús Interestatal")
	bus_transiberia = Autobus(40, "Autobús Transiberià")
	bus_transoceanic = Autobus(30, "Autobús Transoceànic")
	autobusos = [bus_metropolita, bus_interestatal, bus_transiberia, bus_transoceanic]
	return autobusos

def afegir_noms_venda(quantitat_vendre):
	noms = []
	num_bitllets = 0
	while num_bitllets < quantitat_vendre:
		nom = input("Introdueixi el nom del titular d'aquest bitllet: ")
		noms.append(nom)
		num_bitllets += 1
	return noms

def afegir_ids_devolucio(quantitat_retornar):
	ids = []
	num_bitllets = 0
	while num_bitllets < quantitat_retornar:
		id = int(input("Introdueixi el codi d'identificació del bitllet a retornar: "))
		ids.append(id)
		num_bitllets += 1
	return ids
