# -*- coding: utf-8 -*-

menu_autobusos = """
Si us plau, seleccioni l'autobús:
1. Autobús Metropolità
2. Autobús Interestatal
3. Autobús Transiberià
4. Autobus Transoceànic
5. Sortir
"""

menu_accions = """
Seleccioni una acció:
1. Vendre bitllets
2. Retornar bitllets
3. Veure estat de l'autobús
4. Tornar enrere
"""

menu_vendre = "Quants bitllets vols vendre?: "

menu_retornar = "Quants bitllets vols retornar?: "

missatge_operacio_realitzada = "Operació realitzada amb èxit."

numero_no_valid = "Si us plau, introdueixi un número vàlid."
vendre_numero_negatiu = "No pot vendre una quantitat negativa de places."
retornar_numero_negatiu = "No pot retornar una quantitat negativa de places."
vendre_masses_places = "No poden vendre tantes places en aquest autobus."
retornar_masses_places = "No poden retornar tantes places en aquest autobus."

opcio_no_valida = "Si us plau, introdueixi una opció vàlida."