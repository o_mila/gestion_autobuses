# -*- coding: utf-8 -*-
from autobus import *
# Funcions i variables importades
from funcions_principal import *
from variables_principal import *

autobusos = crear_autobusos()

while True:
	try:
		opcio_autobusos = int(input(menu_autobusos))
		if opcio_autobusos == 5:
			print("Sortint de l'aplicatiu...")
			break
		elif opcio_autobusos < 1 or opcio_autobusos > 5:
			print(opcio_no_valida)
		else:
			autobus = autobusos[opcio_autobusos - 1]
			while True:
				try:
					print(autobus.getNom())
					opcio_accions = int(input(menu_accions))
					if opcio_accions == 1:
						while True:
							try:
								quantitat_vendre = int(input(menu_vendre))
								if quantitat_vendre == 0:
									break
								elif quantitat_vendre < 0:
									print(vendre_numero_negatiu)
								elif quantitat_vendre > autobus.getPlacesLliures():
									print(vendre_masses_places)
								else:								
									noms = afegir_noms_venda(quantitat_vendre)
									autobus.venta(quantitat_vendre, noms)
									print(missatge_operacio_realitzada)
									break
							except ValueError:
								print(numero_no_valid)
							except NameError:
								print(numero_no_valid)
							except SyntaxError:
								print(numero_no_valid)
					elif opcio_accions == 2:
						while True:
							try:
								quantitat_retornar = int(input(menu_retornar))
								if quantitat_retornar == 0:
									break
								elif quantitat_retornar < 0:
									print(retornar_numero_negatiu)
								elif quantitat_retornar > autobus.getCapacitat() - autobus.getPlacesLliures():
									print(retornar_masses_places)
								else:
									print("Llista id's actuals: ", autobus.getIds())
									ids = afegir_ids_devolucio(quantitat_retornar)
									autobus.devolucio(quantitat_retornar, ids)
									print(missatge_operacio_realitzada)
									break
							except ValueError:
								print(numero_no_valid)
							except NameError:
								print(numero_no_valid)
							except SyntaxError:
								print(numero_no_valid)
					elif opcio_accions == 3:
						print(autobus)
						for bitllet in autobus.getLlistaBitllets():
							print('Nom: ' + bitllet.getNomUsuari() + ' // id: ' + str(bitllet.getId()))
					elif opcio_accions == 4:
						break
					else:
						print(opcio_no_valida)
				except ValueError:
					print(numero_no_valid)
				except NameError:
					print(numero_no_valid)
				except SyntaxError:
					print(numero_no_valid)
	except ValueError:
		print(numero_no_valid)
	except NameError:
		print(numero_no_valid)
	except SyntaxError:
		print(numero_no_valid)