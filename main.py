from autobus import *
from excepcions import *

def crear_autobusos():
    bus_metropolitano = Autobus(30, "Autobús Metropolitano")
    bus_interestatal = Autobus(50, "Autobús Interestatal")
    bus_transiberià = Autobus(170, "Autobús Transiberià")
    bus_transoceànic = Autobus(130, "Autobús Transoceànic")
    bus_to_the_north = Autobus(12, "Autobús to the North")
    bus_hogwarts = Autobus(600, "Autobús volador to Hogwarts")
    bus_mordor = Autobus(1200, "Autobús to Mordor")
    flota = [bus_metropolitano, bus_interestatal, bus_transiberià, bus_transoceànic, bus_to_the_north, bus_hogwarts, bus_mordor]
    return flota




flota = crear_autobusos()
while True:
    try:
        print("""
    ------------------------------
1. Autobús Metropolitano
2. Autobús Interestatal
3. Autobús Transiberià
4. Autobús Transoceànic
5. Autobús to the North
6. Autobús volador to Hogwarts
7. Autobús to Mordor

0. Sortir
    ------------------------------
    """)
        opcio_autobus = int(input("Introdueixi un autobús: "))

        if opcio_autobus == 0:
            print("Sortint")
            break
        elif opcio_autobus < 0 or opcio_autobus > len(flota):
            print("Opció no valida")
        else:
            autobus = flota[opcio_autobus-1]
            print(autobus.getNom())
    except ValueError:
        print("Si us plau, introdueixi un número vàlid")
    except SyntaxError:
        print("Si us plau, introdueixi un número vàlid")

    while True:
        try:
            print("""
--------------
1. Venda
2. Devolució
3. Estat
4. Tornar enrere
--------------
""")
            opcio_menu = int(input("Indiqui una opció "))
            if opcio_menu == 1:
                while True:
                    try:
                        quantitat = int(input("Indiqui la quantitat de tiquets a comprar: "))
                        if quantitat > autobus.getPlacesLliures():
                            print("Error, no hi han tantes places lliures.")
                        elif quantitat == 0:
                            print("Tornant enrere...")
                            break
                        elif quantitat < 0:
                            print("Error, indiqui una quantitat positiva.")
                        else:
                            contador = 0
                            llista_noms= []
                            while contador < quantitat:
                                nom = input("Indiqui el nom: ")
                                while True:
                                    if len(nom) < 1:
                                        nom = input("El camp no pot estar buit. Indiqui un nom: ")                                        
                                    else:
                                        break
                                llista_noms.append(nom)
                                contador+=1
                            autobus.venta(quantitat, llista_noms) # no cal indicar autubus, ja esta indicat menu_autobus (else)
                            print("Operació realitzada amb éxit")
                            break
                    except ValueError:
                        print("1Si us plau, introdueixi un número vàlid")
                    except NameError:
                        print("2Si us pau, introdueixi un número vàlid")
                    except SyntaxError:
                        print("3Si us plau, introdueixi un número vàlid")
            elif opcio_menu == 2:
                while True:
                    try:
                        quantitat = int(input("Indiqui la quantitat de tiquets a retornar: "))
                        if quantitat > autobus.getCapacitat() - autobus.getPlacesLliures():
                            print("Error, no es poden retornar tants bitllets.")
                        elif quantitat == 0:
                            print("Tornant enrere...")
                            break
                        elif quantitat < 0:
                            print("Error, indiqui una quantitat positiva.")
                        else: 
                            contador = 0
                            llista_ids= []
                            while contador < quantitat:
                                while True:
                                    try:
                                        id = int(input("Indiqui el id del tiquet a retornar: "))
                                        llista_ids.append(id)
                                        contador+=1
                                        break
                                    except ValueError:
                                        print("77Si us plau, introdueixi un número vàlid")
                                    except NameError:
                                        print("882Si us pau, introdueixi un número vàlid")
                                    except SyntaxError:
                                        print("99Si us plau, introdueixi un número vàlid")    
                            autobus.devolucio(quantitat, llista_ids)
                            print("Operació realitzada amb éxit")
                            break
                    except ValueError:
                        print("9Si us plau, introdueixi un número vàlid")
                    except NameError:
                        print("82Si us pau, introdueixi un número vàlid")
                    except SyntaxError:
                        print("7Si us plau, introdueixi un número vàlid")
            elif opcio_menu == 3:
                print(autobus)
                print(llistaPenya(autobus.llista_bitllets))
            elif opcio_menu == 4:
                print("Tornant enrere")
                break
            else:
                print("Introdueixi una opció correcta")
        except ValueError:
            print("4Si us plau, introdueixi un número vàlid")
        except SyntaxError:
            print("6Si us plau, introdueixi un número vàlid")
