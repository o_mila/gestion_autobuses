from excepcions import *
from bitllet import *

class Autobus:
	def __init__(self, capacitat, nom):
		self.setCapacitat(capacitat)
		self.setPlacesLliures(capacitat)
		self.setNom(nom)
		self.llista_bitllets = [] # es pot fer get i set dúnna llista

	def setCapacitat(self, capacitat):
		self.__capacitat = capacitat

	def getCapacitat(self):
		return self.__capacitat

	def setPlacesLliures(self, places_lliures):
		self.__places_lliures = places_lliures

	def getPlacesLliures(self):
		return self.__places_lliures

	def setNom(self, nom):
		self.__nom = nom

	def getNom(self):
		return self.__nom

	def afegirBitllet(self,bitllet):
		self.llista_bitllets.append(bitllet)

	def eliminarBitllet(self, id):
		for index, bitllet in enumerate(self.llista_bitllets):
			if bitllet.getId() == id:
				del self.llista_bitllets[index]



	def __str__(self):
		return (f"""
Total places: {self.getCapacitat()} 
Places lliures: {self.getPlacesLliures()}
Places ocupades: {self.getCapacitat() - self.getPlacesLliures()}
""")



	#__str__

	def venta(self, quantitat, noms):
		# quantitat = #input al menu
		# llista_noms = [] #ja la tenim del main. input al main		
		# EXCEPCIÓ ANIRÀ FORA!!!
		# if quantitat > self.getPlacesLliures():
		# 	raise PlacesInsuficients
			# Falta 
		places_lliures_actualitzades = self.__places_lliures - quantitat
		self.setPlacesLliures(places_lliures_actualitzades)
		max_id  = maxim_id(self.llista_bitllets)  #
		for index, nom_nou in enumerate(noms):
			id = max_id + 1 + index			
			bitllet = Bitllet(nom_nou, id)
			self.afegirBitllet(bitllet)
			



	def devolucio(self, quantitat, ids):
		# quantitat = #input al menu
		# ids ve d'un imput
		# places_ocupades = self.getCapacitat() - self.getPlacesLliures()
				# EXCEPCIÓ ANIRÀ FORA!!!
		# if quantitat > places_ocupades:
		# 	raise PlacesInsuficients
			# Falta 
		places_lliures_actualitzades = self.getPlacesLliures() + quantitat
		self.setPlacesLliures(places_lliures_actualitzades)
		for id in ids:
			self.eliminarBitllet(id)



def maxim_id(llista_bitllets):
	max_id = 0
	for bitllet in llista_bitllets:
		if bitllet.getId() > max:
			max = bitllet.getId()
	return max_id


def llistaPenya(llista_bitllets):
	llista_penya = []
	for bitllet in llista_bitllets:
		passatger = {
			"Nom" : bitllet.getNomUsuari(),
			"Bitllet": bitllet.getId()
}
		llista_penya.append(passatger)
	return llista_penya